/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_H
#define	__VFS_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Internal Includes
#include "path.h"
#include "otype.h"
#include "dirent.h"
#include "handle.h"
#include "mountpoint.h"
#include "obj.h"
#include "obj_io.h"
#include "fs.h"

// Initialize VFS Core
extern void vfs_init();

#endif
