/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_OTYPE_H
#define	__VFS_OTYPE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Object Types
#define	VFS_OTYPE_NONE					0
#define	VFS_OTYPE_FILE					1
#define	VFS_OTYPE_DIR					2

#endif
