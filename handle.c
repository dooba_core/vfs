/* Dooba SDK
 * Virtual File System Core
 */

// External Includes
#include <string.h>
#include <util/str.h>
#include <util/cprintf.h>

// Internal Includes
#include "obj.h"
#include "handle.h"

// Open Handle
uint8_t vfs_open(struct vfs_handle *h, char *path, uint8_t flags)
{
	// Open
	return vfs_open_n(h, path, strlen(path), flags);
}

// Open Handle (Fixed Length)
uint8_t vfs_open_n(struct vfs_handle *h, char *path, uint8_t path_len, uint8_t flags)
{
	struct vfs_mountpoint *mp;
	char *mp_path;
	uint8_t mp_path_len;

	// Parse Path
	if(vfs_mountpoint_parse_path(path, path_len, &mp, &mp_path, &mp_path_len))					{ return 1; }

	// Setup Handle
	h->otype = VFS_OTYPE_NONE;
	h->size = 0;
	h->pos = 0;
	h->mp = mp;

	// Attempt to Open through File System
	if(mp->fs->open(mp, h, mp_path, mp_path_len))
	{
		// Check Flags { Attempt to Create File }
		if((flags & VFS_OPEN_CREATE) == 0)														{ return 1; }
		if(vfs_mkobj_n(path, path_len, VFS_OTYPE_FILE))											{ return 1; }

		// Attempt to Re-Open
		if(mp->fs->open(mp, h, mp_path, mp_path_len))											{ return 1; }
	}
	else
	{
		// Check Flags { Attempt to Truncate File }
		if(flags & VFS_OPEN_TRUNCATE)															{ if(mp->fs->truncate(mp, h, 0)) { mp->fs->close(mp, h); return 1; } }
	}

	// Register Open Handle
	mp->open_handles = mp->open_handles + 1;

	return 0;
}

// Close Handle
void vfs_close(struct vfs_handle *h)
{
	// Close through File System
	h->mp->fs->close(h->mp, h);

	// Drop it
	h->mp->open_handles = h->mp->open_handles - 1;
}
