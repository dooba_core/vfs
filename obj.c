/* Dooba SDK
 * Virtual File System Core
 */

// External Includes
#include <string.h>

// Internal Includes
#include "path.h"
#include "fs.h"
#include "mountpoint.h"
#include "obj_io.h"
#include "obj.h"

// Buffers
uint8_t vfs_obj_cp_buf[VFS_OBJ_CP_BUFSIZE];
char vfs_obj_path_buf[VFS_PATH_MAXLEN];
struct vfs_handle vfs_obj_handle_buf;
struct vfs_handle vfs_obj_handle_buf_tmp;
struct vfs_dirent vfs_obj_dirent_buf;

// Acquire Mountpoint & Sub-Path for Object
uint8_t vfs_obj_acquire_path(char *path, uint8_t path_len, struct vfs_mountpoint **mp, char **mp_path, uint8_t *mp_path_len)
{
	// Parse Path
	if(vfs_mountpoint_parse_path(path, path_len, mp, mp_path, mp_path_len))					{ return 1; }

	return 0;
}

// Create File System Object
uint8_t vfs_mkobj(char *path, uint8_t otype)
{
	// Create
	return vfs_mkobj_n(path, strlen(path), otype);
}

// Create File System Object - Fixed Length
uint8_t vfs_mkobj_n(char *path, uint8_t path_len, uint8_t otype)
{
	struct vfs_mountpoint *mp;
	char *mp_path;
	uint8_t mp_path_len;

	// Acquire Object Mountpoint & Sub-Path
	if(vfs_obj_acquire_path(path, path_len, &mp, &mp_path, &mp_path_len))					{ return 1; }

	// Verify Object doesn't exist
	if(vfs_open_n(&vfs_obj_handle_buf, path, path_len, 0) == 0)
	{
		// Already Exists - Abort
		vfs_close(&vfs_obj_handle_buf);
		return 1;
	}

	// Attempt to Create through File System
	return mp->fs->mkobj(mp, mp_path, mp_path_len, otype);
}

// Destroy File System Object
uint8_t vfs_rmobj(char *path)
{
	// Destroy
	return vfs_rmobj_n(path, strlen(path));
}

// Destroy File System Object - Fixed Length
uint8_t vfs_rmobj_n(char *path, uint8_t path_len)
{
	struct vfs_mountpoint *mp;
	char *mp_path;
	uint8_t mp_path_len;
	uint8_t r;

	// Acquire Object Mountpoint & Sub-Path
	if(vfs_obj_acquire_path(path, path_len, &mp, &mp_path, &mp_path_len))					{ return 1; }

	// Attempt to Open Handle (Verify existence, then, if it's a directory, verify empty)
	if(vfs_open_n(&vfs_obj_handle_buf, path, path_len, 0))									{ return 1; }
	if(vfs_obj_handle_buf.otype != VFS_OTYPE_DIR)											{ vfs_close(&vfs_obj_handle_buf); }
	else
	{
		// Check empty
		r = vfs_readdir(&vfs_obj_handle_buf, &vfs_obj_dirent_buf);
		vfs_close(&vfs_obj_handle_buf);
		if((r) || (vfs_obj_dirent_buf.otype != VFS_OTYPE_NONE))								{ return 1; }
	}

	// Attempt to Destroy through File System
	return mp->fs->rmobj(mp, mp_path, mp_path_len);
}

// Copy File System Object
uint8_t vfs_cpobj(char *src, char *dst)
{
	// Copy
	return vfs_cpobj_n(src, strlen(src), dst, strlen(dst));
}

// Copy File System Object - Fixed Length
uint8_t vfs_cpobj_n(char *src, uint8_t src_len, char *dst, uint8_t dst_len)
{
	struct vfs_mountpoint *src_mp;
	struct vfs_mountpoint *dst_mp;
	char *src_mp_path;
	char *dst_mp_path;
	uint8_t src_mp_path_len;
	uint8_t dst_mp_path_len;
	uint16_t rd;

	// Acquire Object Mountpoints & Sub-Paths
	if(vfs_obj_acquire_path(src, src_len, &src_mp, &src_mp_path, &src_mp_path_len))					{ return 1; }
	if(vfs_obj_acquire_path(dst, dst_len, &dst_mp, &dst_mp_path, &dst_mp_path_len))					{ return 1; }

	// Attempt to Open Destination Handle (Verify it doesn't exist)
	if(vfs_open_n(&vfs_obj_handle_buf, dst, dst_len, 0) == 0)										{ vfs_close(&vfs_obj_handle_buf); return 1; }

	// Attempt to Open Source Handle
	if(vfs_open_n(&vfs_obj_handle_buf, src, src_len, 0))											{ return 1; }

	// Attempt to Create Destination
	if(vfs_open_n(&vfs_obj_handle_buf_tmp, dst, dst_len, VFS_OPEN_CREATE))							{ vfs_close(&vfs_obj_handle_buf); return 1; }

	// Loop
	while(vfs_obj_handle_buf_tmp.size < vfs_obj_handle_buf.size)
	{
		// Copy
		if(vfs_read(&vfs_obj_handle_buf, vfs_obj_cp_buf, VFS_OBJ_CP_BUFSIZE, &rd))					{ goto fail; }
		if(vfs_write(&vfs_obj_handle_buf_tmp, vfs_obj_cp_buf, rd, 0))								{ goto fail; }
	}

	// Close Files
	vfs_close(&vfs_obj_handle_buf_tmp);
	vfs_close(&vfs_obj_handle_buf);

	return 0;

	// On Error
	fail:

	// Close Files
	vfs_close(&vfs_obj_handle_buf_tmp);
	vfs_close(&vfs_obj_handle_buf);

	return 1;
}

// Move File System Object
uint8_t vfs_mvobj(char *src, char *dst)
{
	// Move
	return vfs_mvobj_n(src, strlen(src), dst, strlen(dst));
}

// Move File System Object - Fixed Length
uint8_t vfs_mvobj_n(char *src, uint8_t src_len, char *dst, uint8_t dst_len)
{
	struct vfs_mountpoint *src_mp;
	struct vfs_mountpoint *dst_mp;
	char *src_mp_path;
	char *dst_mp_path;
	uint8_t src_mp_path_len;
	uint8_t dst_mp_path_len;

	// Acquire Object Mountpoints & Sub-Paths
	if(vfs_obj_acquire_path(src, src_len, &src_mp, &src_mp_path, &src_mp_path_len))					{ return 1; }
	if(vfs_obj_acquire_path(dst, dst_len, &dst_mp, &dst_mp_path, &dst_mp_path_len))					{ return 1; }

	// Attempt to Open Source Handle (Verify it exists)
	if(vfs_open_n(&vfs_obj_handle_buf, src, src_len, 0))											{ return 1; }
	vfs_close(&vfs_obj_handle_buf);

	// Attempt to Open Destination Handle (Verify it doesn't exist)
	if(vfs_open_n(&vfs_obj_handle_buf, dst, dst_len, 0) == 0)										{ vfs_close(&vfs_obj_handle_buf); return 1; }

	// Are they on the same mountpoint? { Attempt through File System if move is local }
	if(src_mp == dst_mp)																			{ return src_mp->fs->mvobj(src_mp, src_mp_path, src_mp_path_len, dst_mp_path, dst_mp_path_len); }

	// Copy Object & Destroy
	if(vfs_cpobj_n(src, src_len, dst, dst_len))														{ return 1; }
	return vfs_rmobj_n(src, src_len);
}
