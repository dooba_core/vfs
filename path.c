/* Dooba SDK
 * Virtual File System Core
 */

// External Includes
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "path.h"

// Split Path into Directory & Name
uint8_t vfs_path_split(char *path, uint8_t len, uint8_t *parent_len, uint8_t *name_s, uint8_t *name_len)
{
	uint8_t pplen;
	uint8_t ns;
	uint8_t nl;

	// Split
	if(path[0] != VFS_PATH_DELIM)										{ return 1; }
	if(len == 1)
	{
		// Root Dir
		pplen = 1;
		ns = 1;
		nl = 0;
	}
	else
	{
		// Any other Path
		pplen = str_chr_r(path, len, VFS_PATH_DELIM);
		if((pplen + 2) > len)											{ return 1; }
		ns = pplen + 1;
		nl = len - ns;
		if(pplen == 0)													{ pplen = 1; }
		if(nl > VFS_PATH_ELEMENT_MAXLEN)								{ return 1; }
	}

	// Set Results
	if(parent_len)														{ *parent_len = pplen; }
	if(name_s)															{ *name_s = ns; }
	if(name_len)														{ *name_len = nl; }

	return 0;
}

// Split Full Path (with mountpoint) into Directory & Name
uint8_t vfs_path_split_full(char *path, uint8_t len, uint8_t *parent_len, uint8_t *name_s, uint8_t *name_len)
{
	uint8_t p_s;
	uint8_t p_l;

	// Extract Path
	p_s = str_chr(path, len, VFS_PATH_DELIM);
	if((p_s + 2) > len)													{ return 1; }
	p_l = len - p_s;

	// Split
	if(vfs_path_split(&(path[p_s]), p_l, parent_len, name_s, name_len))	{ return 1; }

	// Add Mountpoint offset
	if(parent_len)														{ *parent_len = *parent_len + p_s; }
	if(name_s)															{ *name_s = *name_s + p_s; }

	return 0;
}
