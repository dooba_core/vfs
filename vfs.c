/* Dooba SDK
 * Virtual File System Core
 */

// External Includes
#include <string.h>

// Internal Includes
#include "vfs.h"

// Initialize VFS Core
void vfs_init()
{
	// Setup File Systems
	vfs_file_systems = 0;

	// Setup Mountpoints
	vfs_mountpoints = 0;
}
