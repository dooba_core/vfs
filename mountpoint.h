/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_MOUNTPOINT_H
#define	__VFS_MOUNTPOINT_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <util/assert.h>
#include <storage/storage.h>

// Internal Includes
#include "otype.h"
#include "fs.h"
#include "handle.h"

// Name Max Length
#define	VFS_MOUNTPOINT_NAME_MAXLEN				8

// File System Data Size
#define	VFS_MOUNTPOINT_DATA_SIZE				64

// Assert File System Data Size
#define	vfs_mountpoint_data_struct(t)			static_assert(sizeof(struct t) <= VFS_MOUNTPOINT_DATA_SIZE, "VFS Mountpoint Data Structure [" #t "] is bigger than VFS_MOUNTPOINT_DATA_SIZE")

// Mountpoint Structure
struct vfs_mountpoint
{
	// Name
	char name[VFS_MOUNTPOINT_NAME_MAXLEN];
	uint8_t name_len;

	// File System Data
	uint8_t data[VFS_MOUNTPOINT_DATA_SIZE];

	// File System
	struct vfs_fs *fs;

	// Storage
	struct storage *st;

	// Open Handles Chain Root
	uint16_t open_handles;

	// Chain
	struct vfs_mountpoint *next;
	struct vfs_mountpoint *prev;
};

// Mountpoints
extern struct vfs_mountpoint *vfs_mountpoints;

// Mount
extern uint8_t vfs_mount(struct vfs_mountpoint *mp, struct storage *st, struct vfs_fs *fs, char *name, uint8_t name_len);

// Mount by File System Name
extern uint8_t vfs_mount_by_fs_name(struct vfs_mountpoint *mp, struct storage *st, char *fs_name, uint8_t fs_name_len, char *name, uint8_t name_len);

// Auto-Mount (Mount with first File System that works)
extern uint8_t vfs_mount_auto(struct vfs_mountpoint *mp, struct storage *st, char *name, uint8_t name_len);

// Unmount
extern uint8_t vfs_unmount(struct vfs_mountpoint *mp);

// Unmount by Name
extern uint8_t vfs_unmount_by_name(char *name, uint8_t name_len);

// Find Mountpoint
extern struct vfs_mountpoint *vfs_mountpoint_find(char *name);

// Find Mountpoint - Fixed Length
extern struct vfs_mountpoint *vfs_mountpoint_find_n(char *name, uint8_t name_len);

// Parse Path
extern uint8_t vfs_mountpoint_parse_path(char *path, uint8_t path_len, struct vfs_mountpoint **mp, char **mp_path, uint8_t *mp_path_len);

#endif
