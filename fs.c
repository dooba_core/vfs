/* Dooba SDK
 * Virtual File System Core
 */

// External Includes
#include <string.h>
#include <util/str.h>
#include <util/cprintf.h>

// Internal Includes
#include "fs.h"

// File System Drivers
struct vfs_fs *vfs_file_systems;

// Register File System
void vfs_register(struct vfs_fs *fs, vfs_fs_mount_t mount, vfs_fs_unmount_t unmount, vfs_fs_mkobj_t mkobj, vfs_fs_rmobj_t rmobj, vfs_fs_mvobj_t mvobj, vfs_fs_open_t open, vfs_fs_close_t close, vfs_fs_truncate_t truncate, vfs_fs_read_t read, vfs_fs_write_t write, vfs_fs_readdir_t readdir, char *name)
{
	// Register File System
	vfs_register_n(fs, mount, unmount, mkobj, rmobj, mvobj, open, close, truncate, read, write, readdir, name, strlen(name));
}

// Register File System - Fixed Length
void vfs_register_n(struct vfs_fs *fs, vfs_fs_mount_t mount, vfs_fs_unmount_t unmount, vfs_fs_mkobj_t mkobj, vfs_fs_rmobj_t rmobj, vfs_fs_mvobj_t mvobj, vfs_fs_open_t open, vfs_fs_close_t close, vfs_fs_truncate_t truncate, vfs_fs_read_t read, vfs_fs_write_t write, vfs_fs_readdir_t readdir, char *name, uint8_t name_len)
{
	// Setup File System
	fs->name_len = csnprintf(fs->name, VFS_FS_NAME_MAXLEN, "%t", name, name_len);
	fs->mount = mount;
	fs->unmount = unmount;
	fs->mkobj = mkobj;
	fs->rmobj = rmobj;
	fs->mvobj = mvobj;
	fs->open = open;
	fs->close = close;
	fs->truncate = truncate;
	fs->read = read;
	fs->write = write;
	fs->readdir = readdir;

	// Register into Chain
	fs->next = vfs_file_systems;
	fs->prev = 0;
	if(vfs_file_systems)													{ vfs_file_systems->prev = fs; }
	vfs_file_systems = fs;
}

// Un-register File System
uint8_t vfs_unregister(struct vfs_fs *fs)
{
	struct vfs_mountpoint *mp;

	// Verify usage
	mp = vfs_mountpoints;
	while(mp)																{ if(mp->fs == fs) { return 1; } mp = mp->next; }

	// Drop it
	if(fs->next)															{ fs->next->prev = fs->prev; }
	if(fs->prev)															{ fs->prev->next = fs->next; }
	if(vfs_file_systems == fs)												{ vfs_file_systems = fs->next; }

	return 0;
}

// Find File System
struct vfs_fs *vfs_fs_find(char *name)
{
	// Find
	return vfs_fs_find_n(name, strlen(name));
}

// Find File System - Fixed Length
struct vfs_fs *vfs_fs_find_n(char *name, uint8_t name_len)
{
	struct vfs_fs *fs;

	// Find
	fs = vfs_file_systems;
	while(fs)																{ if(str_cmp(fs->name, fs->name_len, name, name_len) == 0) { return fs; } fs = fs->next; }

	return 0;
}
