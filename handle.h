/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_HANDLE_H
#define	__VFS_HANDLE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <util/assert.h>

// Internal Includes
#include "path.h"
#include "otype.h"
#include "mountpoint.h"

// Open Flags
#define	VFS_OPEN_CREATE						0x01
#define	VFS_OPEN_TRUNCATE					0x02

// File System Data Size
#define	VFS_HANDLE_DATA_SIZE				32

// Assert Handle Data Size
#define	vfs_handle_data_struct(t)			static_assert(sizeof(struct t) <= VFS_HANDLE_DATA_SIZE, "VFS Handle Data Structure [" #t "] is bigger than VFS_HANDLE_DATA_SIZE")

// Generic Handle Structure
struct vfs_handle
{
	// File System Data
	uint8_t data[VFS_HANDLE_DATA_SIZE];

	// Object Type
	uint8_t otype;

	// Object Size
	uint64_t size;

	// Current Position
	uint64_t pos;

	// Mount Point
	struct vfs_mountpoint *mp;
};

// Open Handle
extern uint8_t vfs_open(struct vfs_handle *h, char *path, uint8_t flags);

// Open Handle (Fixed Length)
extern uint8_t vfs_open_n(struct vfs_handle *h, char *path, uint8_t path_len, uint8_t flags);

// Close Handle
extern void vfs_close(struct vfs_handle *h);

#endif
