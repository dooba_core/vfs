/* Dooba SDK
 * Virtual File System Core
 */

// External Includes
#include <string.h>
#include <util/str.h>
#include <util/cprintf.h>

// Internal Includes
#include "obj_io.h"

// Read from Handle
uint8_t vfs_read(struct vfs_handle *h, void *data, uint16_t size, uint16_t *rd)
{
	// Restrict Size (don't allow reading past the end of file)
	if((h->pos + size) > h->size)												{ size = h->size - h->pos; }

	// Attempt to read through File System
	return h->mp->fs->read(h->mp, h, data, size, rd);
}

// Write to Handle
uint8_t vfs_write(struct vfs_handle *h, void *data, uint16_t size, uint16_t *wr)
{
	// Attempt to write through File System
	return h->mp->fs->write(h->mp, h, data, size, wr);
}

// Get Character from Handle (returns 0 on failure)
uint8_t vfs_getc(struct vfs_handle *h)
{
	uint8_t r;

	// Read single byte
	if(vfs_read(h, &r, 1, 0))													{ return 0; }

	return r;
}

// Put Character to Handle
uint8_t vfs_putc(struct vfs_handle *h, uint8_t c)
{
	// Write single byte
	return vfs_write(h, &c, 1, 0);
}

// Get Line from Handle (does not include '\n')
uint8_t vfs_gets(struct vfs_handle *h, uint8_t *buf, uint16_t bufsize, uint16_t *len)
{
	uint16_t i;
	uint8_t c;

	// Clear
	if(len)																		{ *len = 0; }

	// Loop
	c = 0;
	i = 0;
	while((c != '\n') && (i < bufsize))
	{
		c = vfs_getc(h);
		if(c == 0)																{ return 1; }
		if(c != '\n')															{ buf[i] = c; i = i + 1; }
	}

	// Set Length
	if(len)																		{ *len = i; }

	return 0;
}

// Write Text to Handle
uint8_t vfs_print(struct vfs_handle *h, char *fmt, ...)
{
	va_list ap;
	uint8_t r;
	va_start(ap, fmt);
	r = vfs_print_v(h, fmt, ap);
	va_end(ap);
	return r;
}

// Write Text to Handle (variable argument list)
uint8_t vfs_print_v(struct vfs_handle *h, char *fmt, va_list ap)
{
	struct vfs_obj_io_printer_info pi;

	// Print
	pi.error = 0;
	pi.h = h;
	cvprintf((void (*)(void *, uint8_t))vfs_obj_io_printer, &pi, fmt, ap);
	return pi.error;
}

// Write Line to Handle
uint8_t vfs_puts(struct vfs_handle *h, char *fmt, ...)
{
	va_list ap;
	uint8_t r;

	// Write
	va_start(ap, fmt);
	r = vfs_print_v(h, fmt, ap);
	va_end(ap);
	if(r)																		{ return 1; }

	// Print New Line
	return vfs_putc(h, '\n');
}

// Read next directory entry
uint8_t vfs_readdir(struct vfs_handle *h, struct vfs_dirent *e)
{
	// Attempt to read directory through File System
	return h->mp->fs->readdir(h->mp, h, e);
}

// VFS Object I/O Printer
void vfs_obj_io_printer(struct vfs_obj_io_printer_info *i, uint8_t c)
{
	// Print
	if(i->error)																{ return; }
	if(vfs_putc(i->h, c))														{ i->error = 1; }
}
