/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_OBJ_IO_H
#define	__VFS_OBJ_IO_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>

// Internal Includes
#include "dirent.h"
#include "handle.h"

// VFS Printer Info Structure
struct vfs_obj_io_printer_info
{
	// Handle
	struct vfs_handle *h;

	// Error
	uint8_t error;
};

// Read from Handle
extern uint8_t vfs_read(struct vfs_handle *h, void *data, uint16_t size, uint16_t *rd);

// Write to Handle
extern uint8_t vfs_write(struct vfs_handle *h, void *data, uint16_t size, uint16_t *wr);

// Get Character from Handle (returns 0 on failure)
extern uint8_t vfs_getc(struct vfs_handle *h);

// Put Character to Handle
extern uint8_t vfs_putc(struct vfs_handle *h, uint8_t c);

// Get Line from Handle (does not include '\n')
extern uint8_t vfs_gets(struct vfs_handle *h, uint8_t *buf, uint16_t bufsize, uint16_t *len);

// Write Text to Handle
extern uint8_t vfs_print(struct vfs_handle *h, char *fmt, ...);

// Write Text to Handle (variable argument list)
extern uint8_t vfs_print_v(struct vfs_handle *h, char *fmt, va_list ap);

// Write Line to Handle
extern uint8_t vfs_puts(struct vfs_handle *h, char *fmt, ...);

// Read next directory entry
extern uint8_t vfs_readdir(struct vfs_handle *h, struct vfs_dirent *e);

// VFS Object I/O Printer
extern void vfs_obj_io_printer(struct vfs_obj_io_printer_info *i, uint8_t c);

#endif
