/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_PATH_H
#define	__VFS_PATH_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

/* General Path Structure:
 *
 *   mountpoint:/some_dir/dir2/file.txt
 *
 */

// Maximum Path Length
#ifndef	VFS_PATH_MAXLEN
#define	VFS_PATH_MAXLEN									150
#endif

// Maximum Path Element Length
#ifndef	VFS_PATH_ELEMENT_MAXLEN
#define	VFS_PATH_ELEMENT_MAXLEN							50
#endif

// Mount Point Delimiter
#define	VFS_PATH_MOUNTPOINT_DELIM						':'

// Path Delimiter
#define	VFS_PATH_DELIM									'/'
#define	VFS_PATH_DELIM_STR								"/"

// Split Path into Directory & Name
extern uint8_t vfs_path_split(char *path, uint8_t len, uint8_t *parent_len, uint8_t *name_s, uint8_t *name_len);

// Split Full Path (with mountpoint) into Directory & Name
extern uint8_t vfs_path_split_full(char *path, uint8_t len, uint8_t *parent_len, uint8_t *name_s, uint8_t *name_len);

#endif
