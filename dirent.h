/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_DIRENT_H
#define	__VFS_DIRENT_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Internal Includes
#include "path.h"
#include "otype.h"

// Directory Entry Structure
struct vfs_dirent
{
	// Name
	char name[VFS_PATH_ELEMENT_MAXLEN];
	uint8_t name_len;

	// Object Type
	uint8_t otype;

	// File Size in Bytes
	uint64_t size;
};

#endif
