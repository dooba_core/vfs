/* Dooba SDK
 * Virtual File System Core
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "vfs.h"
#include "scli/mount.h"

// List Mountpoints
void vfs_scli_mount(char **args, uint16_t *args_len)
{
	struct vfs_mountpoint *mp;

	// List Mountpoints
	scli_printf("VFS Mountpoints:\n");
	mp = vfs_mountpoints;
	while(mp)
	{
		// Show
		scli_printf(" * %t on [%t] using '%t'\n", mp->st->name, mp->st->name_len, mp->name, mp->name_len, mp->fs->name, mp->fs->name_len);

		// Next
		mp = mp->next;
	}
}
