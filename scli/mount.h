/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_SCLI_MOUNT_H
#define	__VFS_SCLI_MOUNT_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// List Mountpoints
extern void vfs_scli_mount(char **args, uint16_t *args_len);

#endif
