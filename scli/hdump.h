/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_SCLI_HDUMP_H
#define	__VFS_SCLI_HDUMP_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Chunk Buffer Size
#define	VFS_SCLI_HDUMP_CHUNK_SIZE					16

// Hex Dump VFS File
extern void vfs_scli_hdump(char **args, uint16_t *args_len);

#endif
