/* Dooba SDK
 * Virtual File System Core
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>
#include <util/hdump.h>

// Internal Includes
#include "vfs.h"
#include "scli/hdump.h"

// Hex Dump VFS File
void vfs_scli_hdump(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	struct vfs_handle h;
	uint8_t chunk[VFS_SCLI_HDUMP_CHUNK_SIZE];
	uint8_t csize;
	uint64_t ts;
	uint32_t a;
	uint16_t rs;

	// Acquire Path
	if(str_next_arg(args, args_len, &x, &l) == 0)						{ scli_printf("Missing path\n"); return; }
	if((l == 0) || (l > VFS_PATH_MAXLEN))								{ scli_printf("Invalid path\n"); return; }

	// Read
	a = 0;
	if(vfs_open_n(&h, x, l, 0))											{ scli_printf("Error while opening %t!\n", x, l); return; }
	scli_printf("Reading %t (%Y bytes)\n", x, l, h.size);
	ts = h.size;
	while(ts)
	{
		csize = (ts > VFS_SCLI_HDUMP_CHUNK_SIZE) ? VFS_SCLI_HDUMP_CHUNK_SIZE : ts;
		if(vfs_read(&h, chunk, csize, &rs))								{ goto vfs_scli_hdump_read_error; }
		if(rs != csize)													{ goto vfs_scli_hdump_read_error; }
		hdump(chunk, csize, a, scom_term_printf);
		a = a + csize;
		ts = ts - csize;
	}

	// Normal End
	goto vfs_scli_hdump_end;

	// Read Error
	vfs_scli_hdump_read_error:
	scli_printf("Read failed\n");

	// End
	vfs_scli_hdump_end:
	vfs_close(&h);
}
