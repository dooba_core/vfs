/* Dooba SDK
 * Virtual File System Core
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "vfs.h"
#include "scli/write.h"

// Write VFS File (Truncate)
void vfs_scli_write(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	char *path;
	uint16_t pathlen;
	struct vfs_handle h;
	uint16_t rs;

	// Acquire Path & Data String
	if(str_next_arg(args, args_len, &x, &l) == 0)										{ scli_printf("Missing path\n"); return; }
	if((l == 0) || (l > VFS_PATH_MAXLEN))												{ scli_printf("Invalid path\n"); return; }
	path = x;
	pathlen = l;
	if(str_next_arg(args, args_len, &x, &l) == 0)										{ scli_printf("Missing data string\n"); return; }
	l = str_unesc_n(x, l, 0);

	// Read
	if(vfs_open_n(&h, path, pathlen, VFS_OPEN_CREATE | VFS_OPEN_TRUNCATE))				{ scli_printf("Error while opening %t!\n", x, l); return; }
	scli_printf("Writing %t (%i bytes)\n", path, pathlen, l);
	if(vfs_write(&h, x, l, &rs))														{ goto vfs_scli_write_error; }
	if(l != rs)																			{ goto vfs_scli_write_error; }

	// Normal End
	goto vfs_scli_write_end;

	// Write Error
	vfs_scli_write_error:
	scli_printf("Write failed!\n");

	// End
	vfs_scli_write_end:
	vfs_close(&h);
}
