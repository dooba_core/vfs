/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_SCLI_APPEND_H
#define	__VFS_SCLI_APPEND_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Write VFS File (Append)
extern void vfs_scli_append(char **args, uint16_t *args_len);

#endif
