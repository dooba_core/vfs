/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_SCLI_LS_H
#define	__VFS_SCLI_LS_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Chunk Buffer Size
#define	VFS_SCLI_LS_CHUNK_SIZE					16

// List VFS Contents
extern void vfs_scli_ls(char **args, uint16_t *args_len);

#endif
