/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_SCLI_WRITE_H
#define	__VFS_SCLI_WRITE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <scli/scli.h>

// Write VFS File (Truncate)
extern void vfs_scli_write(char **args, uint16_t *args_len);

#endif
