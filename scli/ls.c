/* Dooba SDK
 * Virtual File System Core
 */

// External Includes
#include <stdlib.h>
#include <string.h>
#include <util/str.h>

// Internal Includes
#include "vfs.h"
#include "scli/mount.h"
#include "scli/ls.h"

// List Contents
void vfs_scli_ls(char **args, uint16_t *args_len)
{
	char *x;
	uint16_t l;
	struct vfs_handle h;
	struct vfs_dirent e;
	uint8_t chunk[VFS_SCLI_LS_CHUNK_SIZE];
	uint8_t csize;
	uint64_t ts;
	uint16_t rs;

	// Acquire Path
	if(str_next_arg(args, args_len, &x, &l) == 0)							{ vfs_scli_mount(args, args_len); return; }
	if(l == 0)																{ vfs_scli_mount(args, args_len); return; }
	if(l > VFS_PATH_MAXLEN)													{ scli_printf("Invalid path\n"); return; }

	// List Contents
	if(vfs_open_n(&h, x, l, 0))												{ scli_printf("Failed to open %t!\n", x, l); return; }
	scli_printf("Reading %t (%Y bytes)\n", x, l, (uint64_t)h.size);
	if(h.otype == VFS_OTYPE_FILE)
	{
		// Display File Contents
		ts = h.size;
		while(ts)
		{
			csize = (ts > VFS_SCLI_LS_CHUNK_SIZE) ? VFS_SCLI_LS_CHUNK_SIZE : ts;
			if(vfs_read(&h, chunk, csize, &rs))								{ goto vfs_scli_ls_read_error; }
			if(rs != csize)													{ goto vfs_scli_ls_read_error; }
			scli_printf("%t", chunk, csize);
			ts = ts - csize;
		}
	}
	else if(h.otype == VFS_OTYPE_DIR)
	{
		// List Directory Contents
		e.otype = VFS_OTYPE_FILE;
		while(e.otype != VFS_OTYPE_NONE)
		{
			if(vfs_readdir(&h, &e))											{ goto vfs_scli_ls_read_error; }
			if(e.otype != VFS_OTYPE_NONE)									{ scli_printf(" * [%c] (%Y) %t\n", ((e.otype == VFS_OTYPE_FILE) ? 'f' : 'd'), e.size, e.name, e.name_len); }
		}
	}
	else																	{ /* NoOp */ }

	// Normal End
	goto vfs_scli_ls_end;

	// Read Error
	vfs_scli_ls_read_error:
	scli_printf("Read failed!\n");

	// End
	vfs_scli_ls_end:
	vfs_close(&h);
}
