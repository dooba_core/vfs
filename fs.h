/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_FS_H
#define	__VFS_FS_H

// External Includes
#include <stdint.h>
#include <stdarg.h>
#include <avr/io.h>
#include <storage/storage.h>

// Internal Includes
#include "otype.h"
#include "handle.h"
#include "dirent.h"
#include "mountpoint.h"

// Name Max Length
#define	VFS_FS_NAME_MAXLEN				8

// Partial Declarations
struct vfs_mountpoint;
struct vfs_handle;

// Mount Method Types
typedef	uint8_t (*vfs_fs_mount_t)(struct vfs_mountpoint *mp, struct storage *st);
typedef	void (*vfs_fs_unmount_t)(struct vfs_mountpoint *mp);

// Directory Control Method Types
typedef	uint8_t (*vfs_fs_mkobj_t)(struct vfs_mountpoint *mp, char *path, uint8_t path_len, uint8_t otype);
typedef	uint8_t (*vfs_fs_rmobj_t)(struct vfs_mountpoint *mp, char *path, uint8_t path_len);
typedef	uint8_t (*vfs_fs_mvobj_t)(struct vfs_mountpoint *mp, char *src, uint8_t src_len, char *dst, uint8_t dst_len);

// Object Handle Control (Open / Close) Method Types
typedef	uint8_t (*vfs_fs_open_t)(struct vfs_mountpoint *mp, struct vfs_handle *h, char *path, uint8_t path_len);
typedef	void (*vfs_fs_close_t)(struct vfs_mountpoint *mp, struct vfs_handle *h);

// Object Handle Access Method Types
typedef	uint8_t (*vfs_fs_truncate_t)(struct vfs_mountpoint *mp, struct vfs_handle *h, uint64_t size);
typedef	uint8_t (*vfs_fs_read_t)(struct vfs_mountpoint *mp, struct vfs_handle *h, void *data, uint16_t size, uint16_t *rd);
typedef	uint8_t (*vfs_fs_write_t)(struct vfs_mountpoint *mp, struct vfs_handle *h, void *data, uint16_t size, uint16_t *wr);
typedef	uint8_t (*vfs_fs_readdir_t)(struct vfs_mountpoint *mp, struct vfs_handle *h, struct vfs_dirent *e);

// Generic File System Structure
struct vfs_fs
{
	// Name
	char name[VFS_FS_NAME_MAXLEN];
	uint8_t name_len;

	// Mount / Unmount
	vfs_fs_mount_t mount;
	vfs_fs_unmount_t unmount;

	// Directory Control
	vfs_fs_mkobj_t mkobj;
	vfs_fs_rmobj_t rmobj;
	vfs_fs_mvobj_t mvobj;

	// Object Handle Control
	vfs_fs_open_t open;
	vfs_fs_close_t close;

	// Object Handle Access
	vfs_fs_truncate_t truncate;
	vfs_fs_read_t read;
	vfs_fs_write_t write;
	vfs_fs_readdir_t readdir;

	// Chain
	struct vfs_fs *next;
	struct vfs_fs *prev;
};

// File System Drivers
extern struct vfs_fs *vfs_file_systems;

// Register File System
extern void vfs_register(struct vfs_fs *fs, vfs_fs_mount_t mount, vfs_fs_unmount_t unmount, vfs_fs_mkobj_t mkobj, vfs_fs_rmobj_t rmobj, vfs_fs_mvobj_t mvobj, vfs_fs_open_t open, vfs_fs_close_t close, vfs_fs_truncate_t truncate, vfs_fs_read_t read, vfs_fs_write_t write, vfs_fs_readdir_t readdir, char *name);

// Register File System - Fixed Length
extern void vfs_register_n(struct vfs_fs *fs, vfs_fs_mount_t mount, vfs_fs_unmount_t unmount, vfs_fs_mkobj_t mkobj, vfs_fs_rmobj_t rmobj, vfs_fs_mvobj_t mvobj, vfs_fs_open_t open, vfs_fs_close_t close, vfs_fs_truncate_t truncate, vfs_fs_read_t read, vfs_fs_write_t write, vfs_fs_readdir_t readdir, char *name, uint8_t name_len);

// Un-register File System
extern uint8_t vfs_unregister(struct vfs_fs *fs);

// Find File System
extern struct vfs_fs *vfs_fs_find(char *name);

// Find File System - Fixed Length
extern struct vfs_fs *vfs_fs_find_n(char *name, uint8_t name_len);

#endif
