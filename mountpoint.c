/* Dooba SDK
 * Virtual File System Core
 */

// External Includes
#include <string.h>
#include <util/str.h>
#include <util/cprintf.h>

// Internal Includes
#include "mountpoint.h"

// Mountpoints
struct vfs_mountpoint *vfs_mountpoints;

// Mount
uint8_t vfs_mount(struct vfs_mountpoint *mp, struct storage *st, struct vfs_fs *fs, char *name, uint8_t name_len)
{
	// Verify if name is available
	if(vfs_mountpoint_find_n(name, name_len))										{ return 1; }

	// Setup Mountpoint
	memset(mp, 0, sizeof(struct vfs_mountpoint));
	mp->name_len = csnprintf(mp->name, VFS_MOUNTPOINT_DATA_SIZE, "%t", name, name_len);
	mp->fs = fs;
	mp->st = st;
	mp->open_handles = 0;

	// Attempt to Mount through File System
	if(fs->mount(mp, st))															{ return 1; }

	// Register into chain
	mp->next = vfs_mountpoints;
	mp->prev = 0;
	if(vfs_mountpoints)																{ vfs_mountpoints->prev = mp; }
	vfs_mountpoints = mp;

	return 0;
}

// Mount by File System Name
uint8_t vfs_mount_by_fs_name(struct vfs_mountpoint *mp, struct storage *st, char *fs_name, uint8_t fs_name_len, char *name, uint8_t name_len)
{
	struct vfs_fs *fs;

	// Find File System
	fs = vfs_fs_find_n(fs_name, fs_name_len);
	if(fs == 0)																		{ return 1; }

	// Mount
	return vfs_mount(mp, st, fs, name, name_len);
}

// Auto-Mount (Mount with first File System that works)
uint8_t vfs_mount_auto(struct vfs_mountpoint *mp, struct storage *st, char *name, uint8_t name_len)
{
	struct vfs_fs *fs;

	// Find first File System that Mounts
	fs = vfs_file_systems;
	while(fs)																		{ if(vfs_mount(mp, st, fs, name, name_len) == 0) { return 0; } fs = fs->next; }

	return 1;
}

// Unmount
uint8_t vfs_unmount(struct vfs_mountpoint *mp)
{
	// Check Open Handles on Mountpoint
	if(mp->open_handles)															{ return 1; }

	// Unmount through File System
	mp->fs->unmount(mp);

	// Synchronize Underlying Storage Device
	storage_sync(mp->st);

	// Drop it
	if(mp->next)																	{ mp->next->prev = mp->prev; }
	if(mp->prev)																	{ mp->prev->next = mp->next; }
	if(vfs_mountpoints == mp)														{ vfs_mountpoints = mp->next; }

	return 0;
}

// Unmount by Name
uint8_t vfs_unmount_by_name(char *name, uint8_t name_len)
{
	struct vfs_mountpoint *mp;

	// Find Mountpoint
	mp = vfs_mountpoint_find_n(name, name_len);
	if(mp == 0)																		{ return 1; }

	// Unmount
	return vfs_unmount(mp);
}

// Find Mountpoint
struct vfs_mountpoint *vfs_mountpoint_find(char *name)
{
	// Find
	return vfs_mountpoint_find_n(name, strlen(name));
}

// Find Mountpoint - Fixed Length
struct vfs_mountpoint *vfs_mountpoint_find_n(char *name, uint8_t name_len)
{
	struct vfs_mountpoint *mp;

	// Find
	mp = vfs_mountpoints;
	while(mp)																		{ if(str_cmp(mp->name, mp->name_len, name, name_len) == 0) { return mp; } mp = mp->next; }

	return 0;
}

// Parse Path
uint8_t vfs_mountpoint_parse_path(char *path, uint8_t path_len, struct vfs_mountpoint **mp, char **mp_path, uint8_t *mp_path_len)
{
	struct vfs_mountpoint *p;
	uint8_t mp_len;

	// Determine Mountpoint
	mp_len = str_chr(path, path_len, VFS_PATH_MOUNTPOINT_DELIM);
	if((mp_len + 2) > path_len)														{ return 1; }

	// Acquire Mountpoint
	p = vfs_mountpoint_find_n(path, mp_len);
	if(p == 0)																		{ return 1; }

	// Set Output
	if(mp)																			{ *mp = p; }
	mp_len = mp_len + 1;
	if(mp_path)																		{ *mp_path = &(path[mp_len]); }
	if(mp_path_len)																	{ *mp_path_len = path_len - mp_len; }

	return 0;
}
