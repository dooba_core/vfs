/* Dooba SDK
 * Virtual File System Core
 */

#ifndef	__VFS_OBJ_H
#define	__VFS_OBJ_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Internal Includes
#include "mountpoint.h"
#include "otype.h"

// Copy Buffer Size
#define	VFS_OBJ_CP_BUFSIZE			16

// Acquire Mountpoint and Sub-Path for Object & Verify accessibility
extern uint8_t vfs_obj_acquire_path(char *path, uint8_t path_len, struct vfs_mountpoint **mp, char **mp_path, uint8_t *mp_path_len);

// Create File System Object
extern uint8_t vfs_mkobj(char *path, uint8_t otype);

// Create File System Object - Fixed Length
extern uint8_t vfs_mkobj_n(char *path, uint8_t path_len, uint8_t otype);

// Destroy File System Object
extern uint8_t vfs_rmobj(char *path);

// Destroy File System Object - Fixed Length
extern uint8_t vfs_rmobj_n(char *path, uint8_t path_len);

// Copy File System Object
extern uint8_t vfs_cpobj(char *src, char *dst);

// Copy File System Object - Fixed Length
extern uint8_t vfs_cpobj_n(char *src, uint8_t src_len, char *dst, uint8_t dst_len);

// Move File System Object
extern uint8_t vfs_mvobj(char *src, char *dst);

// Move File System Object - Fixed Length
extern uint8_t vfs_mvobj_n(char *src, uint8_t src_len, char *dst, uint8_t dst_len);

#endif
